package com.endava.worstclassever;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class WorstClassEverTest {
    private WorstClassEver worstClassEver = new WorstClassEver();


    @Test
    void pressedButtonFalse(){
        final String expectedRes = "please press the button";

        final String res = worstClassEver.worstMethodEver(false, 1, 1);

        assertEquals(expectedRes, res);
    }


    @Test
    void testOlderElephantNormalWeight(){
        final String expectedRes = "This elephant is healthy";

        final String res = worstClassEver.worstMethodEver(true, 40, 250);

        assertEquals(expectedRes, res);
    }


    @Test
    void testOlderElephantLessWeight(){
        final String expectedRes = "This elephant should eat more";

        final String res = worstClassEver.worstMethodEver(true, 40, 99);

        assertEquals(expectedRes, res);
    }


    @Test
    void testOlderElephantMoreWeight(){
        final String expectedRes = "This elephant should eat less";

        final String res = worstClassEver.worstMethodEver(true, 40, 350);

        assertEquals(expectedRes, res);
    }


    @Test
    void testYoungerElephantNormalWeight(){
        final String expectedRes = "This baby elephant is healthy";

        final String res = worstClassEver.worstMethodEver(true, 1, 80);

        assertEquals(expectedRes, res);
    }


    @Test
    void testYoungerElephantLessWeight(){
        final String expectedRes = "This baby elephant should eat more";

        final String res = worstClassEver.worstMethodEver(true, 1, 40);

        assertEquals(expectedRes, res);
    }


    @Test
    void testYoungerElephantMoreWeight(){
        final String expectedRes = "This babyelephant should eat less";

        final String res = worstClassEver.worstMethodEver(true, 1, 100);

        assertEquals(expectedRes, res);
    }



}