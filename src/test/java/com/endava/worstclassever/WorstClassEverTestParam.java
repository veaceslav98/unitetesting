package com.endava.worstclassever;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class WorstClassEverTestParam {
    private boolean passed;
    private int age;
    private double weight;
    private String expectedRes;
    private WorstClassEver worstClassEver;

    @Before
    public void initialize(){
        worstClassEver = new WorstClassEver();
    }

    public WorstClassEverTestParam(boolean passed, int age, double weight, String expectedRes){
        this.passed = passed;
        this.age = age;
        this.weight = weight;
        this.expectedRes = expectedRes;
    }

    @Parameterized.Parameters
    public static Collection values(){
        return Arrays.asList(new Object[][]{
                {false, 1, 1, "please press the button"},
                {true, 40, 250, "This elephant is healthy"},
                {true, 40, 99, "This elephant should eat more"},
                {true, 40, 350, "This elephant should eat less"},
                {true, 1, 80, "This baby elephant is healthy"},
                {true, 1, 40, "This baby elephant should eat more"},
                {true, 1, 100, "This babyelephant should eat less"}
        });
    }


    @Test
    public void testElephantHealth(){
        System.out.printf("isPressed: %b\nAge: %d\nWeight: %.1f\n", passed, age, weight);

        String res = worstClassEver.worstMethodEver(passed, age, weight);

        assertEquals(expectedRes, res);
    }
}