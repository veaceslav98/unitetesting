package com.endava.collections;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class StudentDequeTest {

    private Student student1 = new Student("John1", LocalDate.of(1970, 1, 1), "Student1");;
    private Student student2 = new Student("John2", LocalDate.of(1970, 2, 2), "Student2");;


    @Test
    void testAddFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        actualResult.addFirst(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testAddFirstInCollection(){
        StudentDeque<Student> actualResult = actualResult = new StudentDeque<>();
        actualResult.addFirst(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        actualResult.addFirst(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testAddLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student1, actualResult.getLast());

    }


    @Test
    void testAddLastInCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student2, actualResult.getLast());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testOfferFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.offerFirst(student1);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testOfferFirstInCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addFirst(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.offerFirst(student2);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
        assertEquals(student1, actualResult.getLast());
    }


    @Test
    void testOfferLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.offerLast(student1);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testOfferLastInCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.offerLast(student2);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student2, actualResult.getLast());
    }


    @Test
    void testRemoveFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NullPointerException.class, () -> {
            actualResult.removeFirst();
        });
    }


    @Test
    void testRemoveFirstMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.removeFirst();

        assertTrue(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveFirstMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.removeFirst();

        assertFalse(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testRemoveLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NullPointerException.class, () -> {
            actualResult.removeLast();
        });
    }

    @Test
    void testRemoveLastMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.removeLast();

        assertTrue(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveLastMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        actualResult.removeLast();

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
    }


    @Test
    void testPollFirstMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.pollFirst();

        assertTrue(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPollFirstMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.pollFirst();

        assertFalse(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testPollFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        Student returnValue = actualResult.pollFirst();

        assertNull(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPollLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        Student returnValue = actualResult.pollLast();

        assertNull(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPollLastMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.pollLast();

        assertFalse(actualResult.isEmpty());
        assertEquals(student2, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testGetFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, actualResult::getFirst);
    }


    @Test
    void testGetFirstMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.getFirst();

        assertEquals(student1, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testGetLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, () -> {
            actualResult.getLast();
        });
    }


    @Test
    void testGetLastMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.getLast();

        assertEquals(student2, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testPeekFirstInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        Student returnValue = actualResult.peekFirst();

        assertNull(returnValue);
    }


    @Test
    void testPeekFirstMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.peekFirst();

        assertEquals(student1, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testPeekLastInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        Student returnValue = actualResult.peekLast();

        assertNull(returnValue);
    }


    @Test
    void testPeekLastMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.peekLast();

        assertEquals(student2, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testRemoveMethodWithParams(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.remove(student1);

        assertTrue(returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveMethodWithParamsInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.remove(student1);

        assertFalse(returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveMethodWithParamsIncorrectParam(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.remove(student2);

        assertFalse(returnValue);
        assertEquals(1, actualResult.size());
    }


    @Test
    void testRemoveMethodWithParamsMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.remove(student1);

        assertTrue(returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
        assertEquals(student2, actualResult.getLast());
    }


    @Test
    void testRemoveMethodWithParamsMoreElements2(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.remove(student2);

        assertTrue(returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student1, actualResult.getLast());
    }


    @Test
    void testRemoveFirstOccurrenceWithParams(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.removeFirstOccurrence(student1);

        assertTrue(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveFirstOccurrenceWithParamsIncorrectParam(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.removeFirstOccurrence(student2);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
    }


    @Test
    void testRemoveFirstOccurrenceInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.removeFirstOccurrence(student1);

        assertTrue(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveLastOccurrenceWithParams(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.removeLastOccurrence(student1);


        assertTrue(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveLastOccurrenceWithParamsIncorrectParam(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.removeLastOccurrence(student2);

        assertFalse(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
    }


    @Test
    void testRemoveLastOccurrenceInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.removeLastOccurrence(student1);

        assertFalse(returnValue);
        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveLastOccurrenceMoreParams(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.removeLastOccurrence(student1);

        assertTrue(returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testRemoveLastOccurrenceMoreParams2(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.removeLastOccurrence(student2);

        assertTrue(returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
    }


    @Test
    void testAddMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.add(student1);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
    }


    @Test
    void testAddMethodMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.add(student2);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student2, actualResult.getLast());
    }


    @Test
    void testOfferMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.offer(student1);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
    }


    @Test
    void testOfferMethodMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        boolean returnValue = actualResult.offer(student2);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student2, actualResult.getLast());
    }


    @Test
    void testRemoveInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NullPointerException.class, () -> {
            actualResult.remove();
        });
    }


    @Test
    void testRemoveMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.remove();

        assertTrue(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testRemoveMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.remove();

        assertFalse(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testPollMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, () -> {
            Student returnValue = (actualResult.poll());
            assertNull(returnValue);
        });
    }


    @Test
    void testPollMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.poll();

        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPollMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.poll();

        assertFalse(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testElementInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, actualResult::element);
    }


    @Test
    void testElementMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.element();

        assertEquals(student1, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testPeekMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, actualResult::peek);
    }


    @Test
    void testPeekMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.peek();

        assertEquals(student1, returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testAddAllMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        StudentDeque<Student> addValues = new StudentDeque<>();
        addValues.addLast(student1);
        addValues.addLast(student2);
        boolean returnValue = actualResult.addAll(addValues);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student2, actualResult.getLast());
    }


    @Test
    void testAddAllMethodMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());

        Student student3 = new Student("John3", LocalDate.of(1970, 3, 3), "Student3");;
        Student student4 = new Student("John4", LocalDate.of(1970, 4, 4), "Student4");;
        StudentDeque<Student> addValues = new StudentDeque<>();
        addValues.addLast(student3);
        addValues.addLast(student4);

        boolean returnValue = actualResult.addAll(addValues);

        assertTrue(returnValue);
        assertFalse(actualResult.isEmpty());
        assertEquals(4, actualResult.size());
        assertEquals(student1, actualResult.getFirst());
        assertEquals(student4, actualResult.getLast());
    }


    @Test
    void testClearMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        actualResult.clear();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testClearMethodWithParams(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertEquals(2, actualResult.size());

        actualResult.clear();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPushInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        actualResult.push(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());
        assertEquals(student1, actualResult.getLast());
        assertEquals(student1, actualResult.getFirst());
    }


    @Test
    void testPushInCollection(){
        StudentDeque<Student> actualResult = actualResult = new StudentDeque<>();
        actualResult.addFirst(student1);

        assertFalse(actualResult.isEmpty());
        assertEquals(1, actualResult.size());

        actualResult.push(student2);

        assertFalse(actualResult.isEmpty());
        assertEquals(2, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testPopInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        assertThrows(NoSuchElementException.class, () -> {
            Student returnValue = actualResult.pop();
            assertNull(returnValue);
        });
    }


    @Test
    void testPopMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);

        Student returnValue = actualResult.pop();

        assertTrue(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(0, actualResult.size());
    }


    @Test
    void testPopMoreElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Student returnValue = actualResult.pop();

        assertFalse(actualResult.isEmpty());
        assertEquals(student1, returnValue);
        assertEquals(1, actualResult.size());
        assertEquals(student2, actualResult.getFirst());
    }


    @Test
    void testContainsMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.contains(student1);

        assertFalse(returnValue);
    }


    @Test
    void testContainsWithElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.contains(student1);

        assertTrue(returnValue);
        assertEquals(2, actualResult.size());
    }


    @Test
    void testToArrayMethod(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        Object[] returnValue = actualResult.toArray();

        assertEquals(returnValue[0], actualResult.getFirst());
        assertEquals(returnValue[1], actualResult.getLast());
    }


    @Test
    void testToArrayMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());
        assertEquals(0,actualResult.size());

        Object[] returnValue = actualResult.toArray();

        assertEquals(0, returnValue.length);
    }


    @Test
    void testSizeMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertTrue(actualResult.isEmpty());

        int returnValue = actualResult.size();

        assertEquals(0, returnValue);
    }


    @Test
    void testSizeMethodWithElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertFalse(actualResult.isEmpty());

        int returnValue = actualResult.size();

        assertEquals(2, returnValue);
    }


    @Test
    void testIsEmptyMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertEquals(0, actualResult.size());

        boolean returnValue = actualResult.isEmpty();

        assertTrue(returnValue);
    }


    @Test
    void testIsEmptyMethodWithElements(){
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertEquals(2, actualResult.size());

        boolean returnValue = actualResult.isEmpty();

        assertFalse(returnValue);
    }


    @Test
    void testIteratorMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertEquals(0, actualResult.size());
        assertTrue(actualResult.isEmpty());

        Iterator<Student> iterator = actualResult.iterator();
        boolean returnValue = iterator.hasNext();

        assertFalse(returnValue);

        assertThrows(NullPointerException.class, () -> {
            iterator.next();
        });
    }


    @Test
    void testIteratorMethodWithElements() {
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertEquals(2, actualResult.size());
        assertFalse(actualResult.isEmpty());

        Iterator<Student> iterator = actualResult.iterator();
        boolean returnValue1 = iterator.hasNext();

        assertTrue(returnValue1);

        Student returnValue2 = iterator.next();

        assertEquals(student1, returnValue2);

        Student returnValue3 = iterator.next();

        assertEquals(student2, returnValue3);
    }


    @Test
    void testDescendingIteratorMethodInEmptyCollection(){
        StudentDeque<Student> actualResult = new StudentDeque<>();

        assertEquals(0, actualResult.size());
        assertTrue(actualResult.isEmpty());

        Iterator<Student> descendingIterator = actualResult.descendingIterator();
        boolean returnValue = descendingIterator.hasNext();

        assertFalse(returnValue);

        assertThrows(NullPointerException.class, () -> {
            descendingIterator.next();
        });
    }


    @Test
    void testDescendingIteratorMethodWithElements() {
        StudentDeque<Student> actualResult = new StudentDeque<>();
        actualResult.addLast(student1);
        actualResult.addLast(student2);

        assertEquals(2, actualResult.size());
        assertFalse(actualResult.isEmpty());

        Iterator<Student> descendingIterator = actualResult.descendingIterator();
        boolean returnValue1 = descendingIterator.hasNext();

        assertTrue(returnValue1);

        Student returnValue2 = descendingIterator.next();

        assertEquals(student2, returnValue2);

        Student returnValue3 = descendingIterator.next();

        assertEquals(student1, returnValue3);
    }
}