package com.endava.worstclassevermockito;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
class WorstClassEverMockitoTest {
    private ElephantMessageSupplier elephantMessageSupplier;
    private WorstClassEver worstClassEver;

    @BeforeEach
    void setUp() {
        elephantMessageSupplier = mock(ElephantMessageSupplier.class);
        worstClassEver = new WorstClassEver(elephantMessageSupplier);
    }

    @Test
    void testNotPressedButton() {
        final String expectedRes = "please press the button";

        final String res = worstClassEver.worstMethodEver(false, 1, 1);

        assertEquals(expectedRes, res);
    }


    @Test
    void testGetHealthyElephantMessage() {
        final String expectedResult = "1";
        when(elephantMessageSupplier.getHealthyElephantMessage()).thenReturn("1");

        final String actualResult = worstClassEver.worstMethodEver(true, 40, 250);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getHealthyElephantMessage();
    }


    @Test
    void testGetThinElephantMessage(){
        final String expectedResult = "2";
        when(elephantMessageSupplier.getThinElephantMessage()).thenReturn("2");

        final String actualResult = worstClassEver.worstMethodEver(true, 2, 99);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getThinElephantMessage();
    }


    @Test
    void testGetThickElephantMessage(){
        final String expectedResult = "3";
        when(elephantMessageSupplier.getThickElephantMessage()).thenReturn("3");

        final String actualResult = worstClassEver.worstMethodEver(true, 2, 350);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getThickElephantMessage();
    }


    @Test
    void testGetHealthyBabyElephantMessage(){
        final String expectedResult = "4";
        when(elephantMessageSupplier.getHealthyBabyElephantMessage()).thenReturn("4");

        final String actualResult = worstClassEver.worstMethodEver(true, 1, 60);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getHealthyBabyElephantMessage();
    }


    @Test
    void testGetThinBabyElephantMessage(){
        final String expectedResult = "5";
        when(elephantMessageSupplier.getThinBabyElephantMessage()).thenReturn("5");

        final String actualResult = worstClassEver.worstMethodEver(true, 1, 40);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getThinBabyElephantMessage();
    }


    @Test
    void testGetThickBabyElephantMessage(){
        final String expectedResult = "6";
        when(elephantMessageSupplier.getThickBabyElephantMessage()).thenReturn("6");

        final String actualResult = worstClassEver.worstMethodEver(true, 1, 100);

        assertEquals(expectedResult, actualResult);
        verify(elephantMessageSupplier).getThickBabyElephantMessage();
    }
}