package com.endava.collections;

import java.time.LocalDate;
import java.util.Objects;

public class Student {
    private String name;
    private LocalDate dateOfBirth;
    private String details;

    Student(String name, LocalDate dateOfBirth, String details){
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
    }

    public String getName(){ return name;}

    public LocalDate getDateOfBirth(){ return dateOfBirth;}

    public String getDetails(){ return  details;}

    @Override
    public boolean equals(Object obj){
        if(obj == this){
            return true;
        }

        if(obj == null || obj.getClass() != this.getClass()){
            return false;
        }

        Student student = (Student) obj;
        return name.equals(student.name) && details.equals(student.details) && dateOfBirth.equals(student.dateOfBirth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, dateOfBirth, details);
    }


    @Override
    public String toString(){
        return String.format("%s    %s  %s", name, dateOfBirth, details);
    }

}
