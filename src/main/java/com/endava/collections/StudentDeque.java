package com.endava.collections;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.*;

public class StudentDeque<T> implements Deque<T>{
    private Node<T> head;
    private Node<T>tail;
    private int size = 0;


    private static class Node <T> {
        Node<T> prev;
        Node<T> next;
        T element;
    }

    void printStudents() {
        Iterator<T> iterator = this.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next().toString() + "\n");
        }
    }

    void printStudentReverse() {
        Iterator<T> reverseIterator = this.descendingIterator();
        while (reverseIterator.hasNext()) {
            System.out.print(reverseIterator.next().toString() + "\n");
        }
    }

    private T unlinkFirst(Node<T> first){
        T element = first.element;
        Node<T> node = first.next;
        this.head = node;
        if(node == null){
            this.tail = null;
        }else {
            node.prev = null;
        }
        --this.size;
        return element;
    }

    private T unlinkLast(Node<T> last){
        T element = last.element;
        Node<T> node = last.prev;
        this.tail = node;
        if(node == null){
            this.head = null;
        }else{
            node.next = null;
        }
        --this.size;
        return element;
    }

    @Override
    public void addFirst(T element) {
        Node<T> first = this.head;
        Node<T> node = new Node<>();
        node.element = element;
        node.next = head;
        node.prev = null;
        this.head = node;
        if(first == null){
            this.tail = node;
        }else{
            first.prev = node;
        }
        ++this.size;
    }

    @Override
    public void addLast(T element) {
        Node<T> last = this.tail;
        Node<T> node = new Node<>();
        node.element = element;
        node.next = null;
        node.prev = tail;
        this.tail = node;
        if(last == null){
            this.head = node;
        }else{
            last.next = node;
        }
        ++this.size;
    }

    @Override
    public boolean offerFirst(T element) {
        addFirst(element);
        return true;
    }

    @Override
    public boolean offerLast(T element) {
        addLast(element);
        return true;
    }

    @Override
    public T removeFirst() {
        return this.unlinkFirst(head);
    }

    @Override
    public T removeLast() {
        return this.unlinkLast(tail);
    }

    @Override
    public T pollFirst() {
        return head == null ? null : this.unlinkFirst(head);
    }

    @Override
    public T pollLast() {
        return tail == null ? null : this.unlinkLast(tail);
    }

    @Override
    public T getFirst() {
        if(head == null){
            throw new NoSuchElementException();
        }else {
            return head.element;
        }
    }

    @Override
    public T getLast() {
        if(tail == null){
            throw new NoSuchElementException();
        }else {
            return tail.element;
        }
    }

    @Override
    public T peekFirst() {
        return head == null ? null : head.element;
    }

    @Override
    public T peekLast() {
        return tail == null ? null : tail.element;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        this.remove(o);
        return true;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        for (Node<T> node = this.tail; node != null; node = node.prev) {
            if(node.element.equals(o)){
                T element = node.element;
                Node<T> next = node.next;
                Node<T> prev = node.prev;
                if(prev == null){
                    this.head = next;
                }else{
                    prev.next = next;
                    node.prev = null;
                }
                if(next == null){
                    this.tail = prev;
                }else{
                    next.prev = prev;
                    node.next = null;
                }
                --this.size;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean add(T element) {
        this.addLast(element);
        return true;
    }


    @Override
    public boolean offer(T element) {
        this.addLast(element);
        return true;
    }

    @Override
    public T remove() {
        return this.removeFirst();
    }

    @Override
    public T poll() {
        T element = getFirst();
        this.removeFirst();
        return element;
    }

    @Override
    public T element() {
        return this.getFirst();
    }

    @Override
    public T peek() {
        return this.getFirst();
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        Iterator<? extends T> iterator = collection.iterator();
        while (iterator.hasNext()){
            addLast(iterator.next());
        }
        return true;
    }

    @Override
    public void clear() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    @Override
    public void push(T element) {
        addFirst(element);
    }

    @Override
    public T pop() {
        T element = getFirst();
        this.removeFirst();
        return element;
    }

    @Override
    public boolean remove(Object o) {
        for (Node<T> node = this.head; node != null; node = node.next) {
            if(node.element.equals(o)){
                Node<T> next = node.next;
                Node<T> prev = node.prev;
                if(prev == null){
                    this.head = next;
                }else{
                    prev.next = next;
                    node.prev = null;
                }
                if(next == null){
                    this.tail = prev;
                }else{
                    next.prev = prev;
                    node.next = null;
                }
                --this.size;
                return true;
            }
        }
        return false;
    }


    @Override
    public boolean contains(Object o) {
        Iterator<T> iterator = this.iterator();
        while (iterator.hasNext()){
            if(iterator.hasNext()){
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> first = head;
            @Override
            public boolean hasNext() {
                return first != null;
            }

            @Override
            public T next() {
                T element = first.element;
                first = first.next;
                return element;
            }
        };
    }

    @Override
    public Object[] toArray() {
        Node<T> node;
        int i = 0;
        Object[] arr = new Object[size];
        for(node = this.head; node != null; node = node.next){
            arr[i] = node.element;
            i++;
        }
        return arr;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return new Iterator<T>() {
            Node<T> last = tail;
            @Override
            public boolean hasNext() {
                return last != null;
            }

            @Override
            public T next() {
                T element = last.element;
                last = last.prev;
                return element;
            }
        };
    }


    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }
}
