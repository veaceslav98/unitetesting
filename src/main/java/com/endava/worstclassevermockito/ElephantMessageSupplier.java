/**
 * The only purpose of this class is to supply with messages.
 * There is no algorithms and no calls to instances of another classes here, so there is no reason to test it.
 * (Except of rising the code coverage, but let's just ignore this for now)
 */
package com.endava.worstclassevermockito;

class ElephantMessageSupplier {

    String getHealthyElephantMessage() {
        return "This elephant is healthy";
    }

    String getThickElephantMessage() {
        return "This elephant should eat less";
    }

    String getThinElephantMessage() {
        return "This elephant should eat more";
    }

    String getHealthyBabyElephantMessage() {
        return "This baby elephant is healthy";
    }

    String getThickBabyElephantMessage() {
        return "This baby elephant should eat less";
    }

    String getThinBabyElephantMessage() {
        return "This baby elephant should eat more";
    }
}
