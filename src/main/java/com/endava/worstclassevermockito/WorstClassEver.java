package com.endava.worstclassevermockito;

import com.endava.worstclassevermockito.ElephantMessageSupplier;

class WorstClassEver {

    private final ElephantMessageSupplier elephantMessageSupplier;

    WorstClassEver(final ElephantMessageSupplier elephantMessageSupplier) {
        this.elephantMessageSupplier = elephantMessageSupplier;
    }


    String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight) {

        if (isPressed) {
            if (elephantsAge >= 2) {
                if (elephantsWeight >= 100 && elephantsWeight <= 300) {
                    return elephantMessageSupplier.getHealthyElephantMessage();
                } else if (elephantsWeight < 100) {
                        return elephantMessageSupplier.getThinElephantMessage();
                } else if (elephantsWeight > 300) {
                    return elephantMessageSupplier.getThickElephantMessage();
                }
            } else if (elephantsAge < 2) {
                if (elephantsWeight >= 50 && elephantsWeight <= 90) {
                    return elephantMessageSupplier.getHealthyBabyElephantMessage();
                } else if (elephantsWeight < 50) {
                    return elephantMessageSupplier.getThinBabyElephantMessage();
                } else if (elephantsWeight > 90) {
                    return elephantMessageSupplier.getThickBabyElephantMessage();
                }
            }
        }
        return "please press the button";
    }
}

