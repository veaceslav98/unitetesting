package com.endava.worstclassever;

class WorstClassEver {

    String worstMethodEver(boolean isPressed, int elephantsAge, double elephantsWeight){
        boolean oldElephantAge = elephantsAge >= 2;
        boolean youngerElephantAge = elephantsAge < 2;

        if(isPressed){
            if(oldElephantAge && elephantsWeight >= 100 && elephantsWeight <= 300){
                return "This elephant is healthy";
            }

            if(youngerElephantAge && elephantsWeight >= 50 && elephantsWeight <= 90){
                return "This baby elephant is healthy";
            }

            if(oldElephantAge && elephantsWeight < 100){
                return "This elephant should eat more";
            }

            if(youngerElephantAge && elephantsWeight < 50){
                return "This baby elephant should eat more";
            }

            if(oldElephantAge && elephantsWeight > 300){
                return "This elephant should eat less";
            }

            if(youngerElephantAge && elephantsWeight > 90){
                return "This babyelephant should eat less";
            }
        }
        return "please press the button";
    }
}
